package cz.bsc;

import java.util.List;
import java.util.Random;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import cz.bsc.input.InputHelper;
import cz.bsc.rates.ExchangeRatesLoaderDummyImpl;
import cz.bsc.store.PaymentMemoryStore;
import cz.bsc.store.PaymentsStoreInterface;
import cz.bsc.vo.Balance;
import cz.bsc.vo.Currency;
import cz.bsc.vo.Payment;

/**
 * Some basic tests
 */
public class AppTest extends TestCase {

	private static final Logger logger = Logger.getLogger(AppTest.class);

	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testCurrencyGrouping() {
		PaymentsStoreInterface store = PaymentMemoryStore.getInstance();
		store.init();
		Currency curUsd = new Currency("USD");
		Currency curEur = new Currency("EUR");
		Payment payment1 = new Payment(100, curUsd);
		Payment payment2 = new Payment(200, curUsd);
		Payment payment3 = new Payment(10, curEur);
		store.storePayment(payment1);
		store.storePayment(payment2);
		store.storePayment(payment3);
		List<Balance> list = store.getBalances();
		assertTrue(list.size() == 2);
	}

	public void testZeroBalance() {
		PaymentsStoreInterface store = PaymentMemoryStore.getInstance();
		store.init();
		Currency curUsd = new Currency("USD");
		Integer number = new Random().nextInt();
		Payment payment1 = new Payment(number, curUsd);
		Payment payment2 = new Payment(-number, curUsd);
		store.storePayment(payment1);
		store.storePayment(payment2);
		List<Balance> list = store.getBalances();
		assertTrue(list.get(0).getAmount().intValue() == 0);
	}

	public void testPaymentStore() {
		PaymentsStoreInterface store = PaymentMemoryStore.getInstance();
		store.init();
		Currency curUsd = new Currency("USD");
		Currency curEur = new Currency("EUR");
		Payment payment1 = new Payment(100, curUsd);
		Payment payment2 = new Payment(200, curUsd);
		Payment payment3 = new Payment(10, curEur);
		store.storePayment(payment1);
		store.storePayment(payment2);
		store.storePayment(payment3);
		List<Payment> list = store.getAllPayments();
		assertTrue(list.size() == 3);
	}

	public void testPaymentStoreInit() {
		PaymentsStoreInterface store = PaymentMemoryStore.getInstance();
		store.init();
		assertTrue(store.getAllPayments().size() == 0);
		Currency curUsd = new Currency("USD");
		Payment payment1 = new Payment(100, curUsd);
		store.storePayment(payment1);
		store.storePayment(payment1);
		store.storePayment(payment1);
		store.init();
		assertTrue(store.getAllPayments().size() == 0);
	}

	public void testSingletons() {
		PaymentsStoreInterface store1 = PaymentMemoryStore.getInstance();
		PaymentsStoreInterface store2 = PaymentMemoryStore.getInstance();
		assertEquals(store1, store2);

		ExchangeRatesLoaderDummyImpl impl1 = ExchangeRatesLoaderDummyImpl.getInstance();
		ExchangeRatesLoaderDummyImpl impl2 = ExchangeRatesLoaderDummyImpl.getInstance();
		assertEquals(impl1, impl2);
	}

	public void testInputValidator() {
		assertTrue(InputHelper.isValidPaymentInput("USD 10"));
		assertTrue(InputHelper.isValidPaymentInput("EUR 0"));
		assertTrue(InputHelper.isValidPaymentInput("XXX -20"));
		assertTrue(InputHelper.isValidPaymentInput("USD 2147483647")); // max
																		// value
		assertFalse(InputHelper.isValidPaymentInput("USD 2147483648")); // max
																		// value
																		// +1
		assertFalse(InputHelper.isValidPaymentInput("US 10"));
		assertFalse(InputHelper.isValidPaymentInput(""));
		assertFalse(InputHelper.isValidPaymentInput(null));
		assertFalse(InputHelper.isValidPaymentInput("USSD 10"));
		assertFalse(InputHelper.isValidPaymentInput("USD"));
		assertFalse(InputHelper.isValidPaymentInput("USD "));
		assertFalse(InputHelper.isValidPaymentInput("USD -"));
		assertFalse(InputHelper.isValidPaymentInput(" USD 10"));
		assertFalse(InputHelper.isValidPaymentInput("USD 10 "));
		assertFalse(InputHelper.isValidPaymentInput("USD  10"));
	}

	public void testPrettyBalancePrint() {
		PaymentsStoreInterface store = PaymentMemoryStore.getInstance();
		store.init();
		Currency curUsd = new Currency("USD");
		Currency curEur = new Currency("EUR");
		Payment payment1 = new Payment(100, curUsd);
		Payment payment2 = new Payment(200, curUsd);
		Payment payment3 = new Payment(10, curEur);
		store.storePayment(payment1);
		store.storePayment(payment2);
		store.storePayment(payment3);
		List<Balance> list = store.getBalances();
		for (Balance balance : list) {
			assertTrue(StringUtils.isNotEmpty(balance.getPrettyString()));
		}
	}

	public void testInputFileParser() {
		String fileContent = "USD 100\nxxx\nEUR 50\n\nCZK -10\n";
		List<Payment> list = InputHelper.parseInputFile(fileContent);
		assertTrue(list.size() == 3);

		for (Payment payment : list) {
			if (payment.getCurrency().getCode().equals("USD")) {
				assertEquals(payment.getAmount(), Integer.valueOf(100));
			}
		}

	}

	public void testObjects() {
		try {
			new Payment(10, null);
			fail("It should throw exception");
		} catch (Exception e) {
			logger.error(e);
		}
		try {
			new Payment("xyz");
			fail("It should throw exception");
		} catch (Exception e) {
			logger.error(e);
		}
	}
}
