package cz.bsc.output;

import org.apache.log4j.Logger;

import cz.bsc.store.PaymentsStoreInterface;

/**
 * class for printing balances every minute to std. out
 * 
 * @author Petr Leitner
 * 
 */
public class BalancesPrinter implements Runnable {

	private static final Logger logger = Logger.getLogger(BalancesPrinter.class);

	private final PaymentsStoreInterface paymentStore;

	/**
	 * creates instance of printer, gets payments store which has to be printed
	 * 
	 * @param paymentStore
	 */
	public BalancesPrinter(PaymentsStoreInterface paymentStore) {
		super();
		this.paymentStore = paymentStore;
	}

	@Override
	public void run() {
		while (true) {
			logger.info("Printing balances output");
			System.out.println("Actual balances output:");
			System.out.println(paymentStore.getContentString());
			try {
				// sleep for one minute
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				logger.info("Printer thread interrupted");
				break;
			}
		}
	}

}
