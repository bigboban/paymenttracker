package cz.bsc.vo;

import cz.bsc.input.InputHelper;

/**
 * Value object for one payment
 * 
 * @author Petr Leitner
 * 
 */
public class Payment {

	/**
	 * amount of payment
	 */
	private final int amount;

	/**
	 * currency of payment
	 * 
	 */
	private final Currency currency;

	/**
	 * constructs Payment object from given parameters
	 * 
	 * @param amount
	 * @param currency
	 */
	public Payment(int amount, Currency currency) {
		super();
		if (currency == null) {
			throw new IllegalArgumentException("Currency is null");
		}
		this.amount = amount;
		this.currency = currency;
	}

	/**
	 * constructs Payment object from valid string input, i.e. "USD 100"
	 * 
	 * @param stringPayment
	 */
	public Payment(String stringPayment) {
		super();
		if (!InputHelper.isValidPaymentInput(stringPayment)) {
			throw new IllegalArgumentException("Invalid string input for payment creation");
		}
		String[] input = stringPayment.split(" ");
		this.amount = Integer.valueOf(input[1]);
		this.currency = new Currency(input[0]);
	}

	public Integer getAmount() {
		return amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	@Override
	public String toString() {
		return "Payment [amount=" + amount + ", currency=" + currency + "]";
	}

}
