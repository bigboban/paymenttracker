package cz.bsc.vo;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import cz.bsc.rates.ExchangeRatesLoaderFactory;
import cz.bsc.rates.ExchangeRatesLoaderInterface;

public class Balance extends Payment {

	private static final String USD_CODE = "USD";

	public Balance(Integer amount, Currency currency) {
		super(amount, currency);
	}

	@Override
	public String toString() {
		return "Balance [getAmount()=" + getAmount() + ", toString()=" + super.toString() + "]";
	}

	public String getPrettyString() {
		ExchangeRatesLoaderInterface exchangeRatesLoaderImpl = ExchangeRatesLoaderFactory.getLoaderImpl();
		Rates rates = exchangeRatesLoaderImpl.loadRates();
		StringBuilder sb = new StringBuilder();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.UK);
		symbols.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("#0.##", symbols);
		BigDecimal usdRate = rates.getRates().get(getCurrency());
		sb.append(getCurrency().getCode()).append(" ").append(getAmount());
		if (usdRate != null && !getCurrency().getCode().equals(USD_CODE)) {
			sb.append(" (USD ").append(df.format(new BigDecimal(getAmount()).multiply(usdRate))).append(")");
		}
		return sb.toString();
	}
}
