package cz.bsc.vo;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * class representing exchange rates between some currency and USD
 * 
 * @author Petr Leitner
 * 
 */
public class Rates {

	/**
	 * internal representation of exchange rates
	 * 
	 */
	private final HashMap<Currency, BigDecimal> rates = new HashMap<Currency, BigDecimal>();

	public HashMap<Currency, BigDecimal> getRates() {
		return rates;
	}

}
