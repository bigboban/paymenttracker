package cz.bsc.vo;


/**
 * Value object representing currency
 * 
 * @author Petr Leitner
 * 
 */
public class Currency {

	private final String code;

	public Currency(String code) {
		super();
		if (code == null) {
			throw new IllegalArgumentException("Currency code null");
		}
		if (code.length() != 3) {
			throw new IllegalArgumentException("Currency code length is not 3 characters");
		}
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Currency [code=" + code + "]";
	}
}
