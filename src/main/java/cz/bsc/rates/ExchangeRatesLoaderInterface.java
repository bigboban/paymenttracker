package cz.bsc.rates;

import cz.bsc.vo.Rates;

/**
 * general Exchange rates interface, implementations may load rates from DB,
 * Internet, ..
 * 
 * @author Petr Leitner
 * 
 */
public interface ExchangeRatesLoaderInterface {

	/**
	 * method load rates for available currencies
	 * 
	 * @return
	 */
	public Rates loadRates();

}
