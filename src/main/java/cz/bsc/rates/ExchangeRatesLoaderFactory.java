package cz.bsc.rates;

/**
 * factory for instantiating right exchange loader implementation class
 * 
 * @author Petr Leitner
 * 
 */
public class ExchangeRatesLoaderFactory {

	/**
	 * returns right implementation of exchange rates loader
	 * 
	 * @return
	 */
	public static ExchangeRatesLoaderInterface getLoaderImpl() {
		// some configuration stuff, maybe readed from config file
		// possible returning more implementations

		// for now only one possibility - dummy impl
		return ExchangeRatesLoaderDummyImpl.getInstance();
	}

}
