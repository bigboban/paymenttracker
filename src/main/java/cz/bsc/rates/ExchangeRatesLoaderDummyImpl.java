package cz.bsc.rates;

import java.math.BigDecimal;

import cz.bsc.vo.Currency;
import cz.bsc.vo.Rates;

/**
 * dummy implementation of exchange rates loader - only fake constant values for
 * test purpose
 * 
 * @author Petr Leitner
 * 
 */
public class ExchangeRatesLoaderDummyImpl implements ExchangeRatesLoaderInterface {

	private static ExchangeRatesLoaderDummyImpl instance = null;

	private ExchangeRatesLoaderDummyImpl() {
		super();
	}

	/**
	 * dummy implementation only for testing
	 * 
	 */
	@Override
	public Rates loadRates() {
		Rates rates = new Rates();
		Currency eur = new Currency("EUR");
		Currency czk = new Currency("CZK");
		rates.getRates().put(eur, new BigDecimal(2)); // 1EUR = 2USD
		rates.getRates().put(czk, new BigDecimal(1f / 24f)); // 1CZK = 1/24 USD
		return rates;
	}

	/**
	 * gets instance of ExchangeRatesLoaderDummyImpl singleton
	 * 
	 * @return
	 */
	public static ExchangeRatesLoaderDummyImpl getInstance() {
		if (instance == null) {
			synchronized (ExchangeRatesLoaderDummyImpl.class) {
				if (instance == null) {
					instance = new ExchangeRatesLoaderDummyImpl();
				}
			}
		}
		return instance;
	}

}
