package cz.bsc.store;

import java.util.List;

import cz.bsc.vo.Balance;
import cz.bsc.vo.Payment;

/**
 * interface for general Payments-store
 * 
 * @author Petr Leitner
 * 
 */
public interface PaymentsStoreInterface {

	/**
	 * Initialises Store, i.e. DB store load balances from DB on app start
	 * 
	 */
	void init();

	/**
	 * stores one payment to store
	 * 
	 * @param payment
	 */
	void storePayment(Payment payment);

	/**
	 * - gets balances for all currencies - works on all stored payments -
	 * output is sorted alphabetically by currency code in ascending order
	 * 
	 * @return
	 */
	List<Balance> getBalances();

	/**
	 * return all stored payments
	 * 
	 * @return
	 */
	List<Payment> getAllPayments();

	/**
	 * get text representation of store content
	 * 
	 * @return
	 */
	String getContentString();

	/**
	 * calls init() method and add payments from parameter to store
	 * 
	 * @param initialPayments
	 */
	void init(List<Payment> initialPayments);
}
