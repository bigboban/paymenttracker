package cz.bsc.store;

/**
 * factory for instantiating right payment store implementation class
 * 
 * @author Petr Leitner
 * 
 */
public class PaymentStoreFactory {

	/**
	 * returns right implementation of exchange rates loader
	 * 
	 * @return
	 */
	public static PaymentsStoreInterface getStoreImpl() {
		// some configuration stuff, maybe readed from config file
		// possible returning more implementations

		// for now only one possibility - in-memory store
		return PaymentMemoryStore.getInstance();
	}

}
