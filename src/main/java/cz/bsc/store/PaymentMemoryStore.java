package cz.bsc.store;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import cz.bsc.vo.Balance;
import cz.bsc.vo.Currency;
import cz.bsc.vo.Payment;

/**
 * non-persisent implementation of payments store in memory
 * 
 * @author Petr Leitner
 * 
 */
public class PaymentMemoryStore implements PaymentsStoreInterface {

	private static final Logger logger = Logger.getLogger(PaymentMemoryStore.class);

	private static PaymentMemoryStore instance = null;

	/**
	 * thread safe in-memory representation of payment store
	 */
	private final CopyOnWriteArrayList<Payment> allPayments;

	private PaymentMemoryStore() {
		super();
		logger.info("Creating new instance of PaymentMemoryStore with empty init values");
		this.allPayments = new CopyOnWriteArrayList<Payment>();
		init();
	}

	@Override
	public void init() {
		this.allPayments.clear();
	}

	@Override
	public void init(List<Payment> initialPayments) {
		init();
		this.allPayments.addAll(initialPayments);
	}

	@Override
	public void storePayment(Payment payment) {
		logger.info("Storing payment: " + payment);
		allPayments.add(payment);
	}

	@Override
	public List<Balance> getBalances() {
		HashMap<Currency, Integer> balances = new HashMap<Currency, Integer>();
		for (Payment payment : allPayments) {
			Integer balance = balances.get(payment.getCurrency());
			if (balance == null) {
				balance = Integer.valueOf(0);
			}
			balance = balance + payment.getAmount();
			balances.put(payment.getCurrency(), balance);
		}
		List<Balance> result = new ArrayList<Balance>();
		for (Entry<Currency, Integer> entry : balances.entrySet()) {
			result.add(new Balance(entry.getValue(), entry.getKey()));
		}
		Collections.sort(result, new Comparator<Balance>() {
			@Override
			public int compare(Balance b1, Balance b2) {
				return b1.getCurrency().getCode().compareTo(b2.getCurrency().getCode());
			}
		});
		return result;
	}

	@Override
	public String toString() {
		return "PaymentMemoryStore [allPayments=" + allPayments + "]";
	}

	/**
	 * return text representation of store
	 * 
	 */
	@Override
	public String getContentString() {
		StringBuilder sb = new StringBuilder();
		for (Balance balance : getBalances()) {
			if (balance.getAmount().intValue() != 0) {
				sb.append(balance.getPrettyString());
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	/**
	 * gets instance of memory payment store singleton class
	 * 
	 * @return
	 */
	public static PaymentsStoreInterface getInstance() {
		if (instance == null) {
			synchronized (PaymentMemoryStore.class) {
				if (instance == null) {
					instance = new PaymentMemoryStore();
				}
			}
		}
		return instance;
	}

	@Override
	public CopyOnWriteArrayList<Payment> getAllPayments() {
		return allPayments;
	}

}
