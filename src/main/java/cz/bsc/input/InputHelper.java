package cz.bsc.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import cz.bsc.vo.Payment;

/**
 * helper class for app inputs
 * 
 * @author Petr Leitner
 * 
 */
public class InputHelper {

	private static final Logger logger = Logger.getLogger(InputHelper.class);

	/**
	 * read text file from given location and returns content in one String
	 * object
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String readFile(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}
		reader.close();
		return stringBuilder.toString();
	}

	/**
	 * parses String content of input file if line is valid input it is
	 * converted to Payment object if not line is ignored
	 * 
	 * @param fileContent
	 * @return
	 */
	public static List<Payment> parseInputFile(String fileContent) {
		String[] lines = fileContent.split("\n");
		List<Payment> result = new ArrayList<Payment>();
		int i = 0;
		for (String line : lines) {
			i++;
			String trimmedLine = line.trim();
			boolean validLine = isValidPaymentInput(trimmedLine);
			if (validLine) {
				result.add(new Payment(trimmedLine));
			} else {
				logger.warn("Invalid " + i + ". line.");
			}
		}
		return result;
	}

	/**
	 * validates user input string
	 * 
	 * only valid string is "ABC 100" - first 3 characters capital letters of
	 * English alphabet, one space and number, may be negative with "-" sign,
	 * number must be parsable to Integer java type
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isValidPaymentInput(String input) {
		logger.info("Validating input string: '" + input + "'");
		boolean valid = true;
		if (input == null) {
			logger.info("input string is null");
			return false;
		}
		// minimal length=5, i.e. "USD 1"
		if (input.length() < 5) {
			logger.info("invalid input string length=" + input.length());
			return false;
		}
		int countSpaces = StringUtils.countMatches(input, " ");
		if (countSpaces != 1) {
			logger.info("too many spaces in input string: " + countSpaces);
			return false;
		}
		if (input.charAt(3) != ' ') {
			logger.info("space on invalid position in input string");
			return false;
		}
		String[] values = input.split(" ");
		if (!values[0].matches("[A-Z]{3}")) {
			logger.info("First part of input string not currency: " + values[0]);
			return false;
		}
		if (!values[1].matches("-?\\d{1,}")) {
			logger.info("Second part of input string not number: " + values[1]);
			return false;
		}
		try {
			Integer.valueOf(values[1]);
		} catch (NumberFormatException e) {
			logger.info("Not parsable number: " + values[1]);
			return false;
		}
		logger.info("Input string '" + input + "' is valid");
		return valid;
	}
}
