package cz.bsc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import cz.bsc.input.InputHelper;
import cz.bsc.output.BalancesPrinter;
import cz.bsc.store.PaymentStoreFactory;
import cz.bsc.store.PaymentsStoreInterface;
import cz.bsc.vo.Payment;

/**
 * main Payment tracker application class
 * 
 * @author Petr Leitner
 * 
 */
public class PaymentTracker {

	private static final Logger logger = Logger.getLogger(PaymentTracker.class);
	private static final String QUIT_COMMAND = "quit";

	public static void main(String[] args) {
		logger.info("PaymentTracker application start");
		System.out.println("PaymentTracker application start");

		List<Payment> initialPayments = null;
		if (args.length == 1) {
			logger.info("Input file parameter entered: " + args[0]);
			try {
				String inputFileContent = InputHelper.readFile(args[0]);
				initialPayments = InputHelper.parseInputFile(inputFileContent);
				System.out.println(initialPayments.size() + " initial payments loaded from file " + args[0]);
			} catch (Exception e) {
				logger.warn("Reading of file not possible: " + e);
				System.out.println("Loading from file " + args[0] + " failed");
			}
		} else if (args.length > 1) {
			logger.warn("Invalid number of parameters: " + args.length);
			System.out.println("Invalid number of parameters: " + args.length);
			System.exit(99);
		}

		PaymentsStoreInterface store = PaymentStoreFactory.getStoreImpl();
		if (initialPayments != null) {
			store.init(initialPayments);
		}
		BalancesPrinter printer = new BalancesPrinter(store);
		Thread printerThread = new Thread(printer);
		printerThread.start();
		while (true) {
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			String inputString = null;
			try {
				inputString = bufferRead.readLine();
			} catch (IOException e) {
				logger.error("Unexpected problem with input", e);
				break;
			}

			logger.info("Readed string from user input:" + inputString);
			if (StringUtils.isNotEmpty(inputString) && inputString.equals(QUIT_COMMAND)) {
				break;
			}
			boolean validInput = InputHelper.isValidPaymentInput(inputString);
			if (!validInput) {
				logger.info("Invalid input string entered: " + inputString);
				System.out.println("Invalid input string entered");
				continue;
			}
			System.out.println("Input accepted");
			store.storePayment(new Payment(inputString));
		}
		printerThread.interrupt();
		logger.info("PaymentTracker application end");
	}
}
