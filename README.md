
### Requirements ###

* Maven
* GIT
* Java 1.6+
* Eclipse (optional)

### How do I get set up? ###

1. Standard GIT repository clone
2. Build/package with MVN command. Default "package" goal is set
3. After build app is in TARGET folder
4. App is in PaymentTracker-1.0-SNAPSHOT.jar file
5. Basic app run with "java -jar  PaymentTracker-1.0-SNAPSHOT.jar" command


### Junit tests ###

* To start Junit tests enter 'mvn' or 'mvn package' or 'mvn test' command in place of pom.xml
* It is possible to run test from Eclipse by starting Junit on AppTest.java class

### Application log ###
* Application has lot of logging messages, log filename is PaymentTracker.log
* Output from test is in PaymentTracker-tests.log

### User guide ###

* Start application without arguments or with one argument. Only zero or one argument is valid.
* If one argument is present it represents input file. Input file is in same format as manual input. Empty or invalid lines are ignored. Payments from input file are put to the tracker on application start.
* Only valid "payment" input lines are allowed. Valid currency is 3-chars uppercase and valid value is  integer number (may be negative). Valid examples: "USD 10", "CZK -20". Invalid examples: "usd 10", "xx 20", "USD x", "CZK 1.1".
* When invalid input is entered warning message is displayed, valid input is also confirmed by message.
* Close program by entering "quit" command and enter
* Exchange rates for EUR and CZK currency are included. Because there is lot of currencies and currency code is not limited to valid code (XYZ possible) is is not possible to define exchange rate for all possible currency codes. So only two are defined as example.
* ENJOY this great app :-)
